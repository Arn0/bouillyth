---
title: "Activités"
order: 1
in_menu: true
---
La BouillYth ne vous invite pas à partager vos propres bouillie.

## Nos activités

A titre indicatif, les bouillies proposées :

- De la bouillie de mots ;
- De la bouillie d'idées ;
- De la bouillie de code ;
- De la boullie pour les petits cochons (*Gruiiiick !*) 